var request = require('request'),
    parseString = require('xml2js').parseString;

exports.getTVShows = function(req, res) {

    request('http://thetvdb.com/api/GetSeries.php?seriesname=%20', function (err, response, body) {

        if (err) {

            console.log(err);
            res.send("Error getting data.");
        } else {

            parseString(body, function (err, result) {

                if (err) {

                    console.log(err);
                    res.send("Error parsing data.");
                } else {

                    res.send(result);
                }
            })
        }
    })
}

/*
exports.getCourseById = function(req, res) {
    Course.findOne({_id:req.params.id}).exec(function(err, course) {
        res.send(course);
    })
}*/
