var mongoose = require('mongoose');

var languageSchema = mongoose.Schema({

    name: [String],
    abbreviation: [String],
    id: [String]
});

var Language = mongoose.model('Language', languageSchema);