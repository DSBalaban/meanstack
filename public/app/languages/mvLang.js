angular.module('app').factory('mvLang', function($resource) {

    return $resource('/api/languages', {}, {

        get: {

            method: 'GET',
            cache: true

        }
    });

});