angular.module('app').controller('mvLangCtrl', function($scope, mvLang) {

    $scope.languages = mvLang.get(function(value) {

        $scope.languages = value.Languages.Language;
    });

    $scope.sortOptions = [
        {value:"name",text: "Sort by Language"},
        {value: "id",text: "Sort by ID"}
    ];

    $scope.sortOrder = $scope.sortOptions[0].value;

});