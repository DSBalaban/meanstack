angular.module('app').factory('mvCourse', function($resource) {
  var CourseResource = $resource('/api/courses/:_id', {_id: "@id"}, {
    update: {method:'PUT', isArray:false},
    query: {

      method: 'GET',
      cache: true,
      isArray: true
    }
      }
  );

  return CourseResource;
});