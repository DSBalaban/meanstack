angular.module('app').factory('mvTVShow', function($resource) {

    return $resource('/api/tvshows', {}, {

        get: {

            method: 'GET',
            cache: true

        }
    });

});