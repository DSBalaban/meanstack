angular.module('app').controller('mvTVShowCtrl', function($scope, mvTVShow) {

    $scope.shows = mvTVShow.get(function(value) {

        $scope.shows = value.Data.Series;
    });

    $scope.sortOptions = [
        {value:"SeriesName",text: "Sort by TV Show"},
        {value: "seriesid",text: "Sort by ID"}
    ];

    $scope.sortOrder = $scope.sortOptions[0].value;

});