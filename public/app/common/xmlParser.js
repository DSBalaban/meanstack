angular.module('app').factory('xmlParser', function() {

    var x2js = new X2JS();
    return {

        xml2json: function(data) {

            return x2js.xml_str2json(data);
        }
    }
})

/*redundant, xml parsed by server, see server/utilities/x2jsParser.js*/